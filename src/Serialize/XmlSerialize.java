package Serialize;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;


public class XmlSerialize implements Serializer {
    @Override
    public void serializeList(UniversityList university, String path)  { //List
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(UniversityList.class); //List...
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(university, new File(path));

    }
    catch (JAXBException e) {
        e.printStackTrace();
    }}

    @Override
    public UniversityList deserializeList(String path) throws IOException, JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(UniversityList.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        UniversityList univList = (UniversityList) jaxbUnmarshaller.unmarshal(new File(path));
        return univList;
    }
}
