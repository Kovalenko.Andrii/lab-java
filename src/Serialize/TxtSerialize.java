package Serialize;

import javax.xml.bind.JAXBException;
import java.io.BufferedWriter;
import java.io.*;

public class TxtSerialize implements Serializer {

    @Override
    public UniversityList deserializeList(String path) throws IOException, JAXBException {
        FileInputStream inFile = new FileInputStream(path);
        byte[] str = new byte[inFile.available()];
        inFile.read(str);
        String text = new String(str);
        String delimiter = "\\|";
        String[] mas = text.split(delimiter);
        UniversityList universityList = new UniversityList(mas);
        return universityList;
    }

    @Override
    public void serializeList(UniversityList objsToSerial, String path) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(path));
        writer.write(objsToSerial.toTxtSerial());
        if (writer != null)
            writer.close();

    }
}
