package Serialize;

import com.Model.FullAddress;
import com.Model.University;
import com.Model.accreditationLevel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="UniversityList")
@XmlAccessorType(XmlAccessType.FIELD)
public class UniversityList implements Serializable {
    @XmlElement
    private List<University> list;

    public UniversityList(){
        list = new ArrayList<University>();
    }

    public UniversityList(List<University> list) {
        this.list = list;
    }

    public UniversityList(String[] info){   //rework
        list = new ArrayList<University>();
        for(int i=0;i<info.length;){
            FullAddress infoAddress =  new FullAddress();
            University university = new University();
            university.setId(Integer.parseInt(info[i]));
            university.setName(info[i+1]);
            System.out.print(i);
            university.setYearOfEstablishment(info[i+2]);
            infoAddress.setId(Integer.parseInt(info[i+3]));
            infoAddress.setAddress(info[i+4]);
            infoAddress.setCity(info[i+5]);
            infoAddress.setIndex(Integer.parseInt(info[i+6]));
            university.setAddress(infoAddress);
            university.setLevel(accreditationLevel.valueOf(info[i+7]));
            list.add(university);
            i=i+8;
        }
    }
    public List<University> getList() {
        return list;
    }
    public void setList(List<University> list) {
        this.list = list;
    }

    public String toTxtSerial(){
        String result="";
        for(int i=0;i<list.size();i++){
            result+=list.get(i).toTxtSerial();
        }
        return  result;
    }
    public void add(University u){
        list.add(u);
    }
    @Override
    public String toString() {
        return "UniversityList{" +
                "list=" + list +
                '}';
    }
}
