package Serialize;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public interface Serializer {

    UniversityList deserializeList(String path) throws IOException, JAXBException;

    void serializeList(UniversityList objsToSerial, String path) throws IOException, JAXBException;
}
