package Serialize;

import javax.xml.bind.JAXBException;

import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.core.*;

import java.io.*;
import java.nio.file.*;


public class JsonSerialize implements Serializer {

    @Override
    public UniversityList deserializeList(String path) throws IOException, JAXBException {
        File file = new File(path);
        byte[] mapData = Files.readAllBytes(Paths.get(file.toString()));
        ObjectMapper objectMapper = new ObjectMapper();
        UniversityList univList = objectMapper.readValue(mapData, UniversityList.class);
        return univList;
    }

    @Override
    public void serializeList(UniversityList objsToSerial, String path) throws IOException {
        File file = new File(path);
        ObjectMapper objMapper = new ObjectMapper();
        StringWriter stringWriter = new StringWriter();
        try {
            objMapper.writeValue(stringWriter, objsToSerial);
        } catch ( JsonMappingException e) {
            e.printStackTrace();
        } catch ( JsonGenerationException e ) {
            e.printStackTrace();
        } catch ( IOException e ) {
            e.printStackTrace();
        }
        FileWriter fw = new FileWriter(file);
        fw.write(stringWriter.toString());
        fw.close();
    }
}
