package Servlet;

import Database.UnivertsityDAO;

import com.Model.University;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/edit")
public class EditServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            int id = Integer.parseInt(request.getParameter("id"));
            University university = UnivertsityDAO.findById(id);
            if(university!=null) {
                request.setAttribute("university", university);
                getServletContext().getRequestDispatcher("/edit.jsp").forward(request, response);
            }
            else {
                System.out.println("Yoy");
                getServletContext().getRequestDispatcher("/notfound.jsp").forward(request, response);
            }
        }
        catch(Exception ex) {
            System.out.println(ex);
            getServletContext().getRequestDispatcher("/notfound.jsp").forward(request, response);
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {

            int id = Integer.parseInt(request.getParameter("id"));
            University tempUniversity = UnivertsityDAO.findById(id );
            String name = request.getParameter("name");
            String year = (request.getParameter("year"));
            University university = new University.Builder()
                    .address(tempUniversity.getAddress())
                    .id(id)
                    .yearOfEstablishment(year)
                    .name(name)
                    .accreditationLevel(tempUniversity.getLevel())
                    .build();

            UnivertsityDAO.update(university);
            response.sendRedirect(request.getContextPath() + "/index");
        }
        catch(Exception ex) {

            getServletContext().getRequestDispatcher("/notfound.jsp").forward(request, response);
        }
    }
}