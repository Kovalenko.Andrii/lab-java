package Servlet;

import Database.UnivertsityDAO;
import com.Model.FullAddress;
import com.Model.University;
import com.Model.accreditationLevel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

@WebServlet("/create")
public class CreateServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        getServletContext().getRequestDispatcher("/create.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       try {
           String name = request.getParameter("name");
           String year = request.getParameter("year");

           int id = 14;
           String address = request.getParameter("address");
           String city = request.getParameter("city");
           int index = Integer.parseInt(request.getParameter("index"));

           FullAddress fullAddress = null;
           try {
               fullAddress = new FullAddress.Builder()
                       .id(14)
                       .address(address)
                       .city(city)
                       .index(index)
                       .build();
           } catch (Exception ex) {
               System.out.println(ex.getMessage());
           }
           University university = null;
           try {
               university = new University.Builder()
                       .name(name)
                       .accreditationLevel(accreditationLevel.IV)
                       .address(fullAddress)
                       .yearOfEstablishment(year)
                       .build();
           } catch (Exception ex) {
               System.out.println(ex.getMessage());
           }
           try {
               UnivertsityDAO.insertTable(university);
           } catch (SQLException e) {
               e.printStackTrace();
           } catch (ClassNotFoundException e) {
               e.printStackTrace();
           } catch (InvocationTargetException e) {
               e.printStackTrace();
           } catch (NoSuchMethodException e) {
               e.printStackTrace();
           } catch (InstantiationException e) {
               e.printStackTrace();
           } catch (IllegalAccessException e) {
               e.printStackTrace();
           }
           response.sendRedirect(request.getContextPath() + "/index");
       }
       catch (Exception ex){
           System.out.println(ex);
           getServletContext().getRequestDispatcher("/create.jsp").forward(request, response);
       }
    }
}
