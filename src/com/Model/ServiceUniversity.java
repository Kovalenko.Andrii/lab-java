package com.Model;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
/**
 * Service class containing field <b>serviceListUniversity</b>.
 */
public class ServiceUniversity {

    private List<University> serviceListUniversity;

    /**
     * Method to get field value {@link ServiceUniversity#serviceListUniversity}
     * @return service list university
     */
    public List<University> getListUniversity() {
        return serviceListUniversity;
    }

    /**
     * Method of determining the list of universities {@link ServiceUniversity#serviceListUniversity}
     * @param listUniversity list university
     */
    public void setListUniversity(List<University> listUniversity){
        serviceListUniversity = listUniversity;
    }

    /**
     * Constructor - creating a new object
     * @param listUniversity list university
     */
    public ServiceUniversity(List<University> listUniversity) {
        setListUniversity(listUniversity);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServiceUniversity that = (ServiceUniversity) o;
        return Objects.equals(serviceListUniversity, that.serviceListUniversity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(serviceListUniversity);
    }

    /**
     * University search method by city {@link University#address#city}
     * @param cityFind city for search
     * @return returns a list of universities in the city
     */
    public ArrayList<University> UniversitiesByCity(String cityFind){
        ArrayList<University> universitiesInTheCity = new ArrayList<University>();
        for(int i = 0; i < getListUniversity().size() ; i++){
            if(getListUniversity().get(i).getAddress().getCity().contains(cityFind)){
                universitiesInTheCity.add(getListUniversity().get(i));
            }
        }
        return universitiesInTheCity;
    }
    /**
     * University search method by city {@link University#address#city}
     * using Stream API
     * @param cityFind city for search
     * @return list of universities in the city
     */
    public List<University> UniversityByCityStream(String cityFind) {
    return serviceListUniversity.stream().
            filter(university -> university.getAddress().getCity()==cityFind).
            collect(Collectors.toList());
    }
    /**
     * University search method by accreditation level {@link University#level}
     * @param findAccreditationLevel accreditation level for search
     * @return list of universities at accreditationLevel
     */
    public ArrayList<University> UniversityByLevel (accreditationLevel findAccreditationLevel){
        ArrayList<University> universitiesByLevel = new ArrayList<University>();
        for(int i = 0; i < getListUniversity().size() ; i++){
            if(getListUniversity().get(i).getLevel().equals(findAccreditationLevel)){
                universitiesByLevel.add(getListUniversity().get(i));
            }
        }
        return universitiesByLevel;
    }
    /**
     * University search method by accreditation level {@link University#level}
     * using Stream API
     * @param findAccreditationLevel accreditation level for search
     * @return list of universities at accreditation level
     */
    public List<University> UniversityByLevelStream(accreditationLevel findAccreditationLevel) {
        return serviceListUniversity.stream().
                filter(university -> university.getLevel().equals(findAccreditationLevel)).
                collect(Collectors.toList());
    }
}
