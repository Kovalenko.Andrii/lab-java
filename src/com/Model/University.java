package com.Model;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.*;
import javax.xml.bind.annotation.*;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@XmlRootElement(name="University")
@XmlAccessorType(XmlAccessType.FIELD)
/**
 * University class containing field <b>id</b>, <b>name</b>,<b>address</b>,<b>yearOfEstablishment</b>,<b>level</b>.
 */
public class University implements Serializable {
   // @NotNull(message="Имя должно быть задано")
    @XmlElement
    private int id;
    @XmlElement
    private String name;
    @XmlElement
    private FullAddress address;
    @XmlElement
    private String yearOfEstablishment;
    @XmlElement
    accreditationLevel level;
    /**
     * Builder class containing field <b>name</b>,<b>address</b>,<b>yearOfEstablishment</b>,<b>level</b>.
     * Used to implement the builder pattern
     * Fields of this class are validated.
     */
    public static class Builder{
        @NotNull(message="Id должно быть задано")
        private int id;
        @NotNull(message="Имя должно быть задано")
        private String name;
        @Valid
        private FullAddress address;
        @Pattern(regexp = "^[0-9]+$",message = "Дата должна состоять из цифр")
        @NotNull(message="Дата должна быть задана")
        private String yearOfEstablishment;
        @NotNull(message="Уровень должен быть задан")
        accreditationLevel level;

        public Builder() {
        }
        public Builder id(int valueId){
            id=valueId;
            return this;
        }
        public Builder name(String valueName){
                name=valueName;
                return this;
        }
        public Builder address(FullAddress valueAddres){
            address=valueAddres;
            return this;
        }
        public Builder yearOfEstablishment(String valueYearOfEstablishment){
            yearOfEstablishment=valueYearOfEstablishment;
            return this;
        }
        public Builder accreditationLevel(accreditationLevel valueLevel){
            level = valueLevel;
            return this;
        }

        /**
         * The method that the university class constructor calls if all fields are validated.
         * @return new object class university used constructor
         * @see University#University(Builder)
         * @throws Exception if any of the fields failed validation
         * @see Builder#validate(Object, Validator)
         */
        public University build() throws Exception {
            ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
            Validator validator = vf.getValidator();
           if(validate(this,validator)==0) {
            return new University(this);
           }
            throw new Exception("No build some property University failed validation");//ex
        }

        /**
         * @param object object to check
         * @param validator validator
         * If zero errors, then the validation is considered successful
         * @return the number of fields not validated
         */
        public static int validate(Object object, Validator validator) {
            Set<ConstraintViolation<Object>> constraintViolations = validator
                    .validate(object);
            System.out.println(String.format("Кол-во ошибок: %d",
                    constraintViolations.size()));

            for (ConstraintViolation<Object> cv : constraintViolations)
                System.out.println(String.format(
                        "Внимание, ошибка! property: [%s], value: [%s], message: [%s]",
                        cv.getPropertyPath(), cv.getInvalidValue(), cv.getMessage()));
            return constraintViolations.size();
        }
    }

    /**
     * Filling in University class fields
     * @param builder object of the Builder is validated
     */
    private University(Builder builder) {
        id = builder.id;
        name = builder.name;
        yearOfEstablishment = builder.yearOfEstablishment;
        level = builder.level;
        address = builder.address;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("University{").append("id='").append(getId()).append('\'').append("name='").append(getName()).append('\'').append(", address=").append(getAddress()).append(", yearOfEstablishment=").append(getYearOfEstablishment()).append(", level=").append(getLevel()).append('}').toString();
    }
    public  String toTxtSerial(){
        return  new StringBuilder().append(getId()).append("|").append(getName()).append("|").append(getYearOfEstablishment()).append("|")
                .append(getAddress().toTxtSerial()).append(getLevel()).append("|").toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getYearOfEstablishment() {
        return yearOfEstablishment;
    }

    public void setYearOfEstablishment(String yearOfEstablishment) {
        this.yearOfEstablishment = yearOfEstablishment;
    }

    public accreditationLevel getLevel() { return level; }

    public void setLevel(accreditationLevel level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FullAddress getAddress() {
        return address;
    }

    public void setAddress(FullAddress address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        University that = (University) o;
        return getYearOfEstablishment() == that.getYearOfEstablishment() &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getAddress(), that.getAddress()) &&
                getLevel() == that.getLevel();
    }
    @Override
    public int hashCode() {
        return Objects.hash(getName(), getAddress(), getYearOfEstablishment(), getLevel());
    }


    public University(String name, FullAddress address, String yearOfEstablishment, accreditationLevel level) {
        this.setName(name);
        this.setAddress(address);
        this.setYearOfEstablishment(yearOfEstablishment);
        this.setLevel(level);
    }
    public University(){
        this.setName("");
        //this.setAddress();
        this.setYearOfEstablishment("0");
        this.setLevel(accreditationLevel.IV);
    }
    public University(String[] info){
       FullAddress infoAddress =  new FullAddress();
        this.setId(Integer.parseInt(info[0]));
        this.setName(info[1]);
        this.setYearOfEstablishment(info[2]);
        infoAddress.setAddress(info[3]);
        infoAddress.setCity(info[4]);
        infoAddress.setIndex(Integer.parseInt(info[5]));
        this.setAddress(infoAddress);
        this.setLevel(accreditationLevel.valueOf(info[6]));
    }
}
