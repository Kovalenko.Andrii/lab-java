package com.Model;

import javax.validation.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.*;

import java.util.Objects;
import java.util.Set;
/**
 * FullAddress class containing field <b>id</b>,<b>address</b>,<b>city</b>,<b>index</b>
 * This class is used in the University class as a full address field.
 */
public class FullAddress  {
    private int id;
    private String address;
    private String city;
    private int index;

    public FullAddress() {

    }
    /**
     * Filling in University class fields
     * @param builder object of the Builder is validated
     */
    private FullAddress(FullAddress.Builder builder) {
        id = builder.id;
        address = builder.address;
        city = builder.city;
        index = builder.index;
    }
    public FullAddress(int id, String address, String city, int index) {
        setId(id);
        setAddress(address);
        setCity(city);
        setIndex(index);
    }

    public FullAddress(String address, String city, int index) {
        setAddress(address);
        setCity(city);
        setIndex(index);
    }

    /**
     * @return string contained FullAddress object constructed by the StringBuilder
     * delimiters "|" necessary for dividing an incoming array when deserializing
     * @link Serialize#TxtSerialize
     */
    public  String toTxtSerial(){
        return  new StringBuilder().append(getId()).append("|").append(getAddress())
                .append("|").append(getCity()).append("|")
                .append(getIndex()).append("|").toString();
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
       this.index = index;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FullAddress)) return false;
        FullAddress that = (FullAddress) o;
        return getId() == that.getId() &&
                getIndex() == that.getIndex() &&
                Objects.equals(getAddress(), that.getAddress()) &&
                Objects.equals(getCity(), that.getCity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAddress(), getCity(), getIndex());
    }

    @Override
    public String toString() {
        return new StringBuilder().append(getId()).append(" ").append(getAddress()).append(" ").
                append(getCity()).append(" ").append(getIndex()).append(" ").toString();

    }
    /**
     * Builder class containing field <b>id</b>,<b>address</b>,<b>city</b>,<b>index</b>.
     * Used to implement the builder pattern
     * Fields of this class are validated.
     */
    public static class Builder{
        @NotNull(message="Ид должен быть задан")
        private int id;
        @NotNull(message="Адрес должен быть задан")
        private String address;
        @NotNull(message="Город должен быть задан")
        @Pattern(regexp = "^[a-zA-Z]+$",message = "Название города должно быть одним словом")
        private String city;
        @NotNull(message="Индекс должен быть задан")
        @Min(message = "Длина индекса больше 3", value = 100)
        private int index;

        public Builder() {
        }
        public FullAddress.Builder id(int valueId){
            id=valueId;
            return this;
        }
        public FullAddress.Builder address(String valueAddress){
            address=valueAddress;
            return this;
        }
        public FullAddress.Builder city(String valueCity){
            city=valueCity;
            return this;
        }
        public FullAddress.Builder index(int valueIndex){
           index=valueIndex;
            return this;
        }
        /**
         * The method that the university class constructor calls if all fields are validated.
         * @return new object class full address used constructor
         * @see FullAddress#FullAddress(Builder)
         * @throws Exception if any of the fields failed validation
         * @see Builder#validate(Object, Validator)
         */
        public FullAddress build() throws Exception{
            ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
            Validator validator = vf.getValidator();
            if(validate(this,validator)==0) {
                return new FullAddress(this);}
            throw new Exception("No build some property FullAddress is null");
        }
        /**
         * @param object object to check
         * @param validator validator
         * If zero errors, then the validation is considered successful
         * @return the number of fields not validated
         */
        public static int validate(Object object, Validator validator) {
            Set<ConstraintViolation<Object>> constraintViolations = validator
                    .validate(object);
            System.out.println(String.format("Кол-во ошибок: %d",
                    constraintViolations.size()));

            for (ConstraintViolation<Object> cv : constraintViolations)
                System.out.println(String.format(
                        "Внимание, ошибка! property: [%s], value: [%s], message: [%s]",
                        cv.getPropertyPath(), cv.getInvalidValue(), cv.getMessage()));
            return constraintViolations.size();
        }
    }
}
