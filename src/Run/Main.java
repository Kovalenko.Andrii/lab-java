package Run;
import com.Model.FullAddress;
import com.Model.ServiceUniversity;
import com.Model.University;
import com.Model.accreditationLevel;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<University> universitiesList = new ArrayList<University>();
        universitiesList.add(
                new University("Киевский международный университет (КиМУ)",
                        new FullAddress("ул. Львовская, 49","Киев",03134),
                        "1967",
                        accreditationLevel.IV));
        universitiesList.add(
                new University("Азовский морской институт Одесской национальной морской академии (АМИ ОНМА)",
                        new FullAddress("ул. Черноморская 19","Мариуполь",87500),
                        "1979",
                        accreditationLevel.IV));
        universitiesList.add(
                new University("Академия внутренних войск МВД Украины (АВВУ)",
                        new FullAddress("пл. Восстания 3","Харьков",61000),
                        "1989",
                        accreditationLevel.IV));
        universitiesList.add(
                new University("Винницкий национальный аграрный университет (ВНАУ)",
                        new FullAddress("ул. Солнечная 3","Винница",21000),
                        "1988",
                        accreditationLevel.IV));
        universitiesList.add(
                new University("Донецкий университет экономики и права (ДонУЭП)",
                        new FullAddress("ул. Университетская 77","Донецк",83000),
                        "1979",
                        accreditationLevel.IV));
        universitiesList.add(
                new University("Житомирский государственный университет им. И. Франко (ЖГУ)",
                        new FullAddress("ул. Б. Бердичевская 40","Житомир",10000),
                        "1983",
                        accreditationLevel.IV));
        universitiesList.add(
                new University("Запорожский национальный университет (ЗНУ)",
                        new FullAddress("ул. Жуковского 66","Запорожье",69000),
                        "1978",
                        accreditationLevel.IV));
        universitiesList.add(
                new University("Криворожский педагогический институт КНУ",
                        new FullAddress("пр. Гагарина 54","Кривой Рог",50000),
                        "1978",
                        accreditationLevel.IV));
        universitiesList.add(
                new University("Львовский национальный медицинский университет им. Д. Галицкого (ЛНМУ)",
                        new FullAddress("ул. Пекарская 69","Львов",79000),
                        "1966",
                        accreditationLevel.IV));
        universitiesList.add(
                new University("Международный Славянский университет",
                        new FullAddress("ул. О. Яроша 9-А","Харьков",61000),
                        "1993",
                        accreditationLevel.III));

        ServiceUniversity serviceUniversity;
        serviceUniversity = new ServiceUniversity(universitiesList);
        String cityFind = "Киев";

        System.out.println(serviceUniversity.UniversityByLevel(accreditationLevel.III));
        System.out.println(serviceUniversity.UniversityByLevelStream(accreditationLevel.III));
        System.out.println(serviceUniversity.UniversitiesByCity(cityFind));
        System.out.println(serviceUniversity.UniversityByCityStream(cityFind));
    }
}
