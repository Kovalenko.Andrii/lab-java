package Run;

import com.Model.FullAddress;
import com.Model.University;
import Serialize.*;
import com.Model.accreditationLevel;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public class MainSerial {
        public static void main(String[] args) throws IOException, JAXBException {

            String pathXml;
            String pathTxt;
            String pathJson;
            pathXml  = "serializer." + "xml";
            pathTxt  = "serializer." + "txt";
            pathJson ="serializer." + "json";
            FullAddress ExampleAddress = new FullAddress();
            try {
                        ExampleAddress = new FullAddress.Builder()
                                .id(1)
                                .address("xampleAddr")
                                .city("CityExample")
                                .index(58201)
                                .build();
            }
            catch ( Exception ex ){
                System.out.println(ex.getMessage());
            }
            UniversityList objsToSerial = new UniversityList();
            UniversityList XmlToObjs = new UniversityList();
            UniversityList JsonToObjs = new UniversityList();
            UniversityList TxtToObjs = new UniversityList();
            try {


            objsToSerial.add(new University.Builder()
                    .name("FirstName")
                    .accreditationLevel(accreditationLevel.I)
                    .yearOfEstablishment("1996")
                    .address(ExampleAddress)
                    .build());
            objsToSerial.add(new University.Builder()
                    .name("SecondName")
                    .accreditationLevel(accreditationLevel.II)
                    .yearOfEstablishment("2011")
                    .address(ExampleAddress)
                    .build());
            }
            catch ( Exception ex ){
                System.out.println(ex.getMessage());
            }
            Serializer xmlSerialize = new XmlSerialize();
            xmlSerialize.serializeList(objsToSerial, pathXml);
            XmlToObjs = xmlSerialize.deserializeList(pathXml);

            Serializer jsonSerialize = new JsonSerialize();
            try{
                jsonSerialize.serializeList(objsToSerial,pathJson);
                JsonToObjs = jsonSerialize.deserializeList(pathJson);
            }
            catch ( IOException e) {
                e.printStackTrace();
            }

            Serializer txtSerialize = new TxtSerialize();
            try{
                txtSerialize.serializeList(objsToSerial,pathTxt);
                TxtToObjs = txtSerialize.deserializeList(pathTxt);
            }
            catch (IOException e) {
                e.printStackTrace();
            } catch ( JAXBException e ) {
                e.printStackTrace();
            }

            System.out.print("XMl: ");
            System.out.println(XmlToObjs.toString());
            System.out.print("JSON: ");
            System.out.println(JsonToObjs.toString());
            System.out.print("TXT: ");
            System.out.println(TxtToObjs.toString());
        }
}
