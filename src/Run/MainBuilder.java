package Run;

import com.Model.FullAddress;
import com.Model.University;
import com.Model.accreditationLevel;

public class MainBuilder {
    public static void main(String[] args){
        /*объект создаю раньше чем билдю, но при этом получается что даже если ловлю исключение,
        объект все равно есть.А если создавать объект внутри блока try , то вне его не могу использовать
         */
        FullAddress fullAddress = null;
        University university = new University();
        try {
             fullAddress = new FullAddress.Builder()
                     .id(21)
                     .address("xampleAddr 12")
                     .city("CityExample")
                     .index(548)
                     .build();
        }
        catch ( Exception ex ){
            System.out.println(ex.getMessage());
        }

        try {
                     university = new University.Builder()
                    .name("ExampleName")
                    .accreditationLevel(accreditationLevel.IV)
                    .yearOfEstablishment("1996")
                    .address(fullAddress)
                    .build();
        }
        catch ( Exception ex ){
            System.out.println(ex.getMessage());
        }
        System.out.print(university.toString());


    }

}
