package Database;
import com.Model.FullAddress;

import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class AddressDAO {
    private static String url = "jdbc:mysql://localhost/universitydb?serverTimezone=Europe/Moscow&useSSL=false";
    private static String username = "root";
    private static String password = "mypasswordsql+AZ9";

    private static final String CREATE_TABLE ="CREATE TABLE if not exists address(id  INTEGER PRIMARY KEY AUTO_INCREMENT, address text, city text, indexCity INT );";
    private static final String addAddress = "INSERT INTO address (address, city, indexCity) Values (?, ? ,?)";
    private static final String getAll = "SELECT * FROM address";
    private static final String getByAddress = "SELECT * FROM address WHERE address = ?;";
    private static final String getById = "SELECT * FROM address WHERE id = ?;";
    private static final String deleteByAddress = "DELETE FROM address WHERE address = ?;";
    private static final String deleteById = "DELETE FROM address WHERE id = ?;";

    public static Connection getConnection() throws ClassNotFoundException, SQLException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
        Connection conn = DriverManager.getConnection(Configuration.DB_URL, Configuration.DB_USERNAME, Configuration.DB_PASSWORD);
        System.out.println("Database connection.");
        return conn;
    }
    public static void createTable() throws ClassNotFoundException, SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        AddressDAO.getConnection().prepareStatement(CREATE_TABLE);
        System.out.println("Table created or exist");
    }
    public static int insertTable(FullAddress address) throws SQLException, NullPointerException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        //conn.setAutoCommit(false);
        PreparedStatement preparedStatementAddress = AddressDAO.getConnection().prepareStatement(addAddress,Statement.RETURN_GENERATED_KEYS);
            preparedStatementAddress.setString(1, address.getAddress());
            preparedStatementAddress.setString(2, address.getCity());
            preparedStatementAddress.setInt(3,    address.getIndex());
            preparedStatementAddress.addBatch();
        int [] count=preparedStatementAddress.executeBatch();
       // conn.commit();
        System.out.println("Record added");
        int generatedkey=0;
        ResultSet resultSet = preparedStatementAddress.getGeneratedKeys();
        if (resultSet.next()) {
            generatedkey=resultSet.getInt(1);
            System.out.println("Auto Generated Primary Key " + generatedkey);
        }
        return generatedkey;
    }

    public static List<FullAddress> getAll() throws ClassNotFoundException, SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        ResultSet resultSet;
        Statement statmt =  AddressDAO.getConnection().createStatement();
        resultSet = statmt.executeQuery(getAll);
        List<FullAddress> addressList = new ArrayList<FullAddress>();
        while(resultSet.next())
        {   int id = resultSet.getInt("id");
            int index = resultSet.getInt("indexCity");
            String  city = resultSet.getString("city");
            String  address = resultSet.getString("address");
            try {
                addressList.add(new FullAddress.Builder()
                        .id(id)
                        .address(address)
                        .city(city)
                        .index(index)
                        .build());
            }
            catch ( Exception ex ){
                System.out.println(ex.getMessage());
            }
        }
        System.out.println("Table output");
        return addressList;
    }
    public static List<FullAddress> findByAddress(String addressFind) throws ClassNotFoundException, SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        List<FullAddress> addressList = new ArrayList<FullAddress>();
        ResultSet resSet;
        PreparedStatement preparedStatementAddress = AddressDAO.getConnection().prepareStatement(getByAddress);
        preparedStatementAddress.setString(1, addressFind);
        resSet = preparedStatementAddress.executeQuery();
        while (resSet.next()) {
            int id = resSet.getInt("id");
            int index = resSet.getInt("indexCity");
            String city = resSet.getString("city");
            String address = resSet.getString("address");
            try {
                addressList.add(new FullAddress.Builder()
                        .id(id)
                        .address(address)
                        .city(city)
                        .index(index)
                        .build());
            }
            catch ( Exception ex ){
                System.out.println(ex.getMessage());
            }
        }
        return addressList;
    }
    public static FullAddress findById(int idFind) throws ClassNotFoundException, SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        FullAddress addressFind = new FullAddress();
        ResultSet resSet;
        PreparedStatement preparedStatementAddress = AddressDAO.getConnection().prepareStatement(getById);
        preparedStatementAddress.setInt(1, idFind);
        resSet = preparedStatementAddress.executeQuery();
        while (resSet.next()) {
            int id = resSet.getInt("id");
            int index = resSet.getInt("indexCity");
            String city = resSet.getString("city");
            String address = resSet.getString("address");
            try {
                addressFind = new FullAddress.Builder()
                        .id(id)
                        .address(address)
                        .city(city)
                        .index(index)
                        .build();
            }
            catch ( Exception ex ){
                System.out.println(ex.getMessage());
            }
        }
        return addressFind;
    }
    public static void deleteByAddress(String addressDelete) throws ClassNotFoundException, SQLException
    {   try {
        PreparedStatement preparedStatementAddress = AddressDAO.getConnection().prepareStatement(deleteByAddress);
        AddressDAO.getConnection().setAutoCommit(false);
        preparedStatementAddress.setString(1, addressDelete);
        preparedStatementAddress.addBatch();
        int[] count = preparedStatementAddress.executeBatch();
//        AddressDAO.getConnection().commit();
        System.out.println("Record delete");
        }
        catch ( Exception ex ) {
            System.out.println(ex.getMessage());
        }
    }
    public static void deleteById(int deleteId) throws ClassNotFoundException, SQLException
    {   try {
        PreparedStatement preparedStatementAddress = AddressDAO.getConnection().prepareStatement(deleteById);
        AddressDAO.getConnection().setAutoCommit(false);
        preparedStatementAddress.setInt(1, deleteId);
        preparedStatementAddress.addBatch();
        int[] count = preparedStatementAddress.executeBatch();
//        AddressDAO.getConnection().commit();
        System.out.println("Record delete");
    }
    catch ( Exception ex ) {
        System.out.println(ex.getMessage());
    }
    }
}
