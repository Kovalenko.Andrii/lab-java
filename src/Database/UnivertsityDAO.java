package Database;

import com.Model.FullAddress;
import com.Model.University;
import com.Model.accreditationLevel;


import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UnivertsityDAO {
    private static final String createTable = "CREATE TABLE if not exists university " +
            "(id  INTEGER PRIMARY KEY AUTOINCREMENT, address INTEGER," +
            " name TEXT, yearOfEstablishment TEXT ,level TEXT," +
            "FOREIGN KEY (address) REFERENCES address (id));";
    private static final String addUniversity =  "INSERT INTO university " +
            "(address, name , YearOfEstablishment , level) VALUES (?,?,?,?); ";
    private static final String getAll = "SELECT * FROM university";
    private static final String deleteById = "DELETE FROM university WHERE id = ?;";
    private static final String getUniversityByLevel = "SELECT * FROM university WHERE level=?";
    private static final String getUniversityByCity = "SELECT * FROM university WHERE address IN \n"+
            "(SELECT id FROM address WHERE city= ? );";
    private static final String getById = "SELECT * FROM university WHERE id = ?;";
    private static final String update = "UPDATE university SET name = ?, yearOfEstablishment = ? WHERE id = ?;";

    public static Connection getConnection() throws ClassNotFoundException, SQLException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
        Connection conn = DriverManager.getConnection(Configuration.DB_URL, Configuration.DB_USERNAME, Configuration.DB_PASSWORD);
        System.out.println("Database connection.");
        return conn;
    }

    public static void createTable() throws ClassNotFoundException, SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        UnivertsityDAO.getConnection().prepareStatement(createTable);
        System.out.println("Table created or exist.");
    }
       public static int insertTable(University university) throws SQLException, NullPointerException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        int idAddress=0;
        idAddress = AddressDAO.insertTable(university.getAddress());
        PreparedStatement preparedStatementUniversity = UnivertsityDAO.getConnection().prepareStatement(addUniversity,Statement.RETURN_GENERATED_KEYS);
        preparedStatementUniversity.setInt(1, idAddress);
        preparedStatementUniversity.setString(2, university.getName());
        preparedStatementUniversity.setString(3,    university.getYearOfEstablishment());
        preparedStatementUniversity.setString(4,    university.getLevel().toString());
        preparedStatementUniversity.addBatch();
        int [] count=preparedStatementUniversity.executeBatch();
        System.out.println("Record added");
        int generatedKey=0;
        ResultSet rs=preparedStatementUniversity.getGeneratedKeys();
        if (rs.next()) {
            generatedKey=rs.getInt(1);
            System.out.println("Auto Generated Primary Key " + generatedKey);
        }
        return generatedKey;
    }
    public static List<University> getAll() throws ClassNotFoundException, SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        ResultSet resultSet;
        Statement statmt =  UnivertsityDAO.getConnection().createStatement();
        resultSet = statmt.executeQuery(getAll);
        List<University> universityList = new ArrayList<University>();
        while(resultSet.next())
        {   int id = resultSet.getInt("id");
            int address = resultSet.getInt("address");
            String  name = resultSet.getString("name");
            String  yearOfEstablishment = resultSet.getString("yearOfEstablishment");
            String  level = resultSet.getString("level");
            try {
                universityList.add(new University.Builder()
                        .id(id)
                        .address(AddressDAO.findById(address))
                        .name(name)
                        .yearOfEstablishment(yearOfEstablishment)
                        .accreditationLevel(accreditationLevel.valueOf(level))
                        .build());
            }
            catch ( Exception ex ){
                System.out.println(ex.getMessage());
            }
        }
        System.out.println("Table output");
        return universityList;
    }
    public static List<University> getUniversityByLevel(String levelFind) throws ClassNotFoundException, SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        List<University> universityList = new ArrayList<University>();
        ResultSet resultSet;
        PreparedStatement preparedStatementAddress = AddressDAO.getConnection().prepareStatement(getUniversityByLevel);
        preparedStatementAddress.setString(1, levelFind);
        resultSet = preparedStatementAddress.executeQuery();
        while(resultSet.next())
        {   //int id = resultSet.getInt("id");
            int address = resultSet.getInt("address");
            String  name = resultSet.getString("name");
            String  yearOfEstablishment = resultSet.getString("yearOfEstablishment");
            String  level = resultSet.getString("level");
            try {
                universityList.add(new University.Builder()
                        .address(AddressDAO.findById(address))
                        .name(name)
                        .yearOfEstablishment(yearOfEstablishment)
                        .accreditationLevel(accreditationLevel.valueOf(level))
                        .build());
            }
            catch ( Exception ex ){
                System.out.println(ex.getMessage());
            }
        }
        return universityList;
    }
    public static University findById(int idFind) throws ClassNotFoundException, SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        University universityFind = new University();
        ResultSet resSet;
        PreparedStatement preparedStatementUniversity = UnivertsityDAO.getConnection().prepareStatement(getById);
        preparedStatementUniversity.setInt(1, idFind);
        resSet = preparedStatementUniversity.executeQuery();
        while (resSet.next()) {
            int id = resSet.getInt("id");
            int address = resSet.getInt("address");
            String name = resSet.getString("name");
            String year = resSet.getString("yearOfEstablishment");
            accreditationLevel level = accreditationLevel.valueOf( resSet.getString("level"));
            try {
                universityFind = new University.Builder()
                        .id(id)
                        .name(name)
                        .yearOfEstablishment(year)
                        .accreditationLevel(level)
                        .address(AddressDAO.findById(address))
                        .build();
            }
            catch ( Exception ex ){
                System.out.println(ex.getMessage());
            }
        }
        return universityFind;
    }
    public static List<University> getUniversityByCity(String cityFind) throws ClassNotFoundException, SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        ResultSet resultSet;
        PreparedStatement preparedStatementAddress = UnivertsityDAO.getConnection().prepareStatement(getUniversityByCity);
        AddressDAO.getConnection();
        preparedStatementAddress.setString(1, cityFind);
        resultSet = preparedStatementAddress.executeQuery();

        List<University> universityList = new ArrayList<University>();
        while(resultSet.next())
        {   //int id = resultSet.getInt("id");
            int address = resultSet.getInt("address");
            String  name = resultSet.getString("name");
            String  yearOfEstablishment = resultSet.getString("yearOfEstablishment");
            String  level = resultSet.getString("level");
            try {
                universityList.add(new University.Builder()
                        .address(AddressDAO.findById(address))
                        .name(name)
                        .yearOfEstablishment(yearOfEstablishment)
                        .accreditationLevel(accreditationLevel.valueOf(level))
                        .build());
            }
            catch ( Exception ex ){
                System.out.println(ex.getMessage());
            }
        }
        System.out.println("Table output");
        return universityList;
    }
    public static  void update(University university) throws NoSuchMethodException, IllegalAccessException, InstantiationException, SQLException, InvocationTargetException, ClassNotFoundException {
        PreparedStatement preparedStatementAddress = UnivertsityDAO.getConnection().prepareStatement(update);
        preparedStatementAddress.setString(1,university.getName());
        preparedStatementAddress.setString(2,university.getYearOfEstablishment());
        preparedStatementAddress.setInt(3,university.getId());
        preparedStatementAddress.executeUpdate();
    }
    public static void deleteById(int deleteId) throws ClassNotFoundException, SQLException
    {   try {
        PreparedStatement preparedStatementAddress = UnivertsityDAO.getConnection().prepareStatement(deleteById);
        UnivertsityDAO.getConnection().setAutoCommit(false);
        preparedStatementAddress.setInt(1, deleteId);
        preparedStatementAddress.addBatch();
        int[] count = preparedStatementAddress.executeBatch();
//        AddressDAO.getConnection().commit();
        System.out.println("Record delete");
        }
        catch ( Exception ex ) {
            System.out.println(ex.getMessage());
        }
    }
}
