package TestUniver;
import com.Model.ServiceUniversity;
import com.Model.University;
import com.Model.FullAddress;
import com.Model.accreditationLevel;
import org.testng.Assert;
import org.testng.annotations.Test;


import java.util.ArrayList;
import java.util.List;

public class TestServiceUniversity {
    @Test
    public void testUniversitiesInTheCity() {
        List<University> universitiesListForTest = new ArrayList<University>();
        universitiesListForTest.add(
                new University("Киевский международный университет (КиМУ)",
                        new FullAddress("ул. Львовская, 49","Киев",03134),
                        "1967",
                        accreditationLevel.IV));
        universitiesListForTest.add(
                new University("Азовский морской институт Одесской национальной морской академии (АМИ ОНМА)",
                        new FullAddress("ул. Черноморская 19","Мариуполь",87500),
                        "1979",
                        accreditationLevel.IV));

        ServiceUniversity serviceUniversityForTest;
        serviceUniversityForTest = new ServiceUniversity(universitiesListForTest);

        List<University> UniversitiesListForResultTest = new ArrayList<>();
        UniversitiesListForResultTest.add(
                new University("Киевский международный университет (КиМУ)",
                        new FullAddress("ул. Львовская, 49","Киев",03134),
                        "1967",
                        accreditationLevel.IV));
        Assert.assertEquals( UniversitiesListForResultTest , serviceUniversityForTest.UniversitiesByCity("Киев")  );
    }

}
