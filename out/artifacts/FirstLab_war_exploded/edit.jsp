<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 10.12.2018
  Time: 1:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Change information</title>
</head>
<body>
<h3>Edit university</h3>
<form method="post">
    <input type="hidden" value="${university.id}" name="id" />
    <label>Name</label><br>
    <input name="name" value="${university.name}" /><br><br>
    <label>Year</label><br>
    <input name="year" value="${university.yearOfEstablishment}" /><br><br>
    <input type="submit" value="Send" />
</form>
</body>
</html>