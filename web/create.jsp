<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 09.12.2018
  Time: 23:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add university</title>
</head>
<body>
<h3>New university</h3>
<form method="post">
    <label>Name</label><br>
    <input name="name"/><br><br>

    <label>Year</label><br>
    <input name="year"/><br><br>

    <label>Address</label><br>
    <input name="address"/><br><br>

    <label>City</label><br>
    <input name="city"/><br><br>

    <label>Index</label><br>
    <input name="index" type="number"/><br><br>

    <input type="submit" value="Save" />
</form>
</body>
</html>
