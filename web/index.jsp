<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 09.12.2018
  Time: 22:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Universities</title>
</head>
<body>
<h2>Universities List</h2>
<p><a href='<c:url value="/create" />'>Create new</a></p>
<table>
  <tr><th>Name</th><th>Address</th><th>Year</th><th>Level</th><th></th></tr>
  <c:forEach var="university" items="${universities}">
    <tr><td>${university.name}</td>
      <td>${university.address}</td>
      <td>${university.yearOfEstablishment}</td>
      <td>${university.level}</td>
      <td>
        <a href='<c:url value="/edit?id=${university.id}" />'>Edit</a> |

        <form method="post" action='<c:url value="/delete" />' style="display:inline;">
          <input type="hidden" name="id" value="${university.id}">
          <input type="submit" onclick="return confirm('This is delete ?')" value="Delete">
        </form>
      </td></tr>
  </c:forEach>
</table>
</body>
</html>
